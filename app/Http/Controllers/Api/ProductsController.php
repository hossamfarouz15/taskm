<?php

namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function products(){
        $products = Product::all();
        if (count($products)  > 0){
            $data = [];
            foreach ($products as $product){
                $arr['id']= $product->id;
                $arr['name'] = $product->name;
                $arr['product_owner'] = $product->User->name;
                $arr['image'] = asset('images/users/'.$product->image);
                $data[]=$arr;
            }
           return response()->json(['value'=>true,'data'=>$data]);
        }else{
            return response()->json(['value'=>false,'msg'=>'there are no products']);
        }
    }
}
