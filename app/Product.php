<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['user_id', 'name', 'image'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
